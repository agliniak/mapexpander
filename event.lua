
event = {}
event.type = "none"
event.arg1 = 0
event.arg2 = 0

function Event:new(type,arg1,arg2)
	local newEvent= {}
	setmetatable(newEvent, self)
	self.__index = self

	newEvent.type = type
	newEvent.arg1 = arg1
	newEvent.arg2 = arg2
end