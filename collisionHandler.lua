CollisionHandler = {}

function CollisionHandler:new(player, levelRects)
	local newCollisionHandler = {}
	setmetatable(newCollisionHandler, self)
	self.__index = self

	newCollisionHandler.player = player
	newCollisionHandler.levelRects = levelRects

	return newCollisionHandler
end

function CollisionHandler:GetRelativeVelocity(vectorA,vectorB)


	local relativeV= Vector2d:new()
	relativeV.x=vectorB.x-vectorA.x
	relativeV.y=vectorB.y-vectorA.y
	return relativeV
end

function CollisionHandler:GetNormal(vector)

	local mag = vector:magnitude()
	return vector:divideout(mag)
end

function CollisionHandler:GetDotProduct(vectorA,vectorB)

	return (vectorA.x*vectorB.x)+(vectorA.y*vectorB.y)
end

function CollisionHandler:GetPenRect(rectA,rectB)
--non minkowski
	local rectC = {}
	rectC.x1 = math.max(rectA.x,rectB.x) 
	rectC.y1 = math.max(rectA.y,rectB.y) 
	rectC.x2 = math.min(rectA.x+rectA.w,rectB.x+rectB.w) 
	rectC.y2 = math.min(rectA.y+rectA.h,rectB.y+rectB.h) 
	rectC.w = rectC.x2-rectC.x1
	rectC.h = rectC.y2-rectC.y1
	return rectC
end

function CollisionHandler:checkPlayerVSLevelRects()
	


	for i=1, table.getn(self.levelRects) do


		rectC=self:MinkowskiDifference(self.player.hitBox,self.levelRects[i])
--just to make third rect visible
self.levelRects[i].minkowski.x=rectC.x
self.levelRects[i].minkowski.y=rectC.y
self.levelRects[i].minkowski.w=rectC.w
self.levelRects[i].minkowski.h=rectC.h

		
		if self:MinkowskiHitOrigin(rectC) then
			self.player.collide = "right"
			
			minDist = math.abs(0-rectC.x)
			escapeV = Vector2d:new(rectC.x,0)
			escapeV.x=1
			escapeV.y=0
			minAxis = "x"


			if math.abs(rectC.x2-0) < minDist then
				minDist = math.abs(rectC.x2-0)
				--escapeV.x=rectC.x2
				escapeV.x=-1
				escapeV.y=0

				minAxis = "x"
				self.player.collide = "left"
			end
			if math.abs(rectC.y2-0) < minDist then
				minDist = math.abs(rectC.y2-0)
				--escapeV.y=rectC.y2
				escapeV.x=0
				escapeV.y=-1
				minAxis = "y"
				
				--self.player.grounded=true
				self.player.collide = "bottom"
			end
			if math.abs(rectC.y-0) < minDist then
				minDist = math.abs(rectC.y-0)
				--escapeV.y=rectC.y
				escapeV.x=0
				escapeV.y=1
				minAxis = "y"
				self.player.collide = "top"
			end

			if minAxis == "x" then

				self.player.hitBox.x=self.player.hitBox.x+(escapeV.x*minDist)
				self.player.velocity.x=0
				self.player.acceleration.x=0
			elseif minAxis == "y" then
					self.player.hitBox.y=self.player.hitBox.y+(escapeV.y*minDist)
				self.player.velocity.y=0
				self.player.acceleration.y=0

			end
			print(self.player.collide)

		
		end
		


	end
end

function CollisionHandler:update()

	self:checkPlayerVSLevelRects()
end

-- function CollisionHandler:DistanceBtwCenters(rectA,rectB)

	
-- 	local platformCenter={}
-- 	local playerCenter ={}
-- 	local distanceBtwCenters ={}
-- 	playerCenter.y = (rectA.y+(rectA.y+rectA.h))/2
-- 	platformCenter.y = (rectB.y+(rectB.y+rectB.h))/2

-- 	playerCenter.x = (rectA.x+(rectA.x+rectA.w))/2
-- 	platformCenter.x = (rectB.x+(rectB.x+rectB.w))/2

-- 	distanceBtwCenters.y=playerCenter.y-platformCenter.y
-- 	distanceBtwCenters.x=playerCenter.x-platformCenter.x
-- 	--if distanceBtwCenterY is Negative then its from top
-- 	--if distanceBteCenterY is Positice then its from bottem
-- 	--id distancebtwCenterX is Negative then its from the left
-- 	--if distacebtwCenterX is Postive then its from the right

-- 	return distanceBtwCenters

-- end

function CollisionHandler:MinkowskiDifference(rectA,rectB)

	local rectC = {}
	rectC.x = rectA.x-(rectB.x+rectB.w)
	rectC.y = rectA.y-(rectB.y+rectB.h)
	rectC.w=rectA.w+rectB.w
	rectC.h=rectA.h+rectB.h
	rectC.x2 = rectC.x + rectC.w
	rectC.y2 = rectC.y + rectC.h

	return rectC
end

function CollisionHandler:MinkowskiHitOrigin(rectC)

	if rectC.x<=0 and
		rectC.x2>=0 and
		rectC.y<=0 and
		rectC.y2>=0 then

	
		print("minkowski Collision!")
	
		return true
	else
    	return false
	end
end

function CollisionHandler:GetPenVector(rectC)
	--getting the penetration vector of the minkowski rect angle and 0,0
	local origin = {}
	origin.x=0
	origin.y=0

	--object A from right
	minDist= math.abs(rectC.x-0)
	penVector=Vector2d:new(1,0) 
	--print("pen="..rectC.x)

	--if from left
	if math.abs(rectC.x2-0)< minDist then
		minDist=math.abs(rectC.x2-0)
		--print("pen="..rectC.x2)
		penVector.x=-1
		penVector.y=0

	end


	-- if math.abs(rectC.y-0)<minDist then
	-- 	minDist=rectC.y
	-- 	penVector.x=0
	-- 	penVector.y=-minDist
	-- end

	-- if math.abs(rectC.y2-0)<minDist then
	-- 	minDist=rectC.y2
	-- 	penVector.x=0
	-- 	penVector.y=minDist

	-- end


	
	--the pen vector is the minimum distance from the originto the minkowski rect.
	return penVector
end



function CollisionHandler:AIntersectB(rectA,rectB)

	--if every thing is true then there is an intersection
	if rectA.x < rectB.x+rectB.w and
   		rectA.x+rectA.w > rectB.x and
  		rectA.y < rectB.y+rectB.h and
   		rectA.y+rectA.h > rectB.y then

  		return true
	else
		
		return false
	end
end

-- function CollisionHandler:CheckDirection(distanceBtwCenters)

-- 	if distanceBtwCenters.x < 0 then
-- 		print("From Left!")

-- 		elseif distanceBtwCenters.x > 0 then
-- 			print("From Right")

-- 	end
-- 	if distanceBtwCenters.y < 0 then
-- 		print("From Top!")

-- 		elseif distanceBtwCenters.y > 0 then
-- 			print("From Bottem")

-- 	end
-- 	--get centers,
-- 	--measure distance,
-- 	--positive and negative can help direction?



-- end



-- (-1,0) left face normal
-- (1,0) right face normal
-- (0,-1) bottem face normal
-- (0,1) top face normal