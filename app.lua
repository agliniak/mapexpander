App = {}

function App:new()
	local newApp = {}
	setmetatable(newApp, self)
	self.__index = self

	return newApp
end

function App:load()
	math.randomseed(os.time())
	love.filesystem.setIdentity("tilegen")
	--make array of 1900 for 16x16 tiles on 800x600 with randomw 0-255
	-- screenTiles = {}
	-- tileIndex = 1
	-- for rowIndex = 0, 50-1 do
	-- 	for columnIndex = 0, 38-1 do 
	-- 		screenTiles[tileIndex] = math.random(0,255)
	-- 		tileIndex=tileIndex+1
	-- 	end
	-- end

	--try with a multidemsional array
	rowRes = 50
	colRes = 38


	--player
	player = {}
	player.x = 25
	player.y = 16
	--world coords for all tiles to cache, center, and surrounding.
	player.worldCache = { {player.x,player.y},
						  {player.x+1,player.y},
						  {player.x+1,player.y+1},
						  {player.x,player.y+1},
						  {player.x-1,player.y+1},
						  {player.x-1,player.y},
						  {player.x-1,player.y-1},
						  {player.x,player.y-1},
						  {player.x+1,player.y-1} }











--before = collectgarbage("count")

	screenTiles2 = {}
	
	for rowIndex = 1, rowRes do
		screenTiles2[rowIndex] = {}
		for columnIndex = 1, colRes do 
			
			screenTiles2[rowIndex][columnIndex] = math.random(0,255)
		end

	end
-- after = collectgarbage("count")
-- print("before: "..before)
-- print("after: "..after)

-- print(after-before)
	

	--print multidemsional display
	-- for rowIndex = 1, rowRes do
		
	-- 	for columnIndex = 1, colRes do 
			
	-- 		io.write(screenTiles2[rowIndex][columnIndex]..",")
	-- 	end
	-- 	print()
	-- end


numberOfAverages = 5

realdeepwater = 110
deepwater = 120
watercutoff=128

sandcutoff = 131
lightgrass = 135
darkgrass = 140
lowermount = 145
highmount = 159


dirt = 132

for i =0, numberOfAverages do
	for rowIndex = 1, rowRes do
		
		for columnIndex = 1, colRes do 
			
			--io.write(screenTiles2[rowIndex][columnIndex]..",")
			local oldValue = screenTiles2[rowIndex][columnIndex]
			local average = 0
			local NumstoAvg = 0

			if      rowIndex == rowIndex and columnIndex + 1 <= colRes then 
						average= average + screenTiles2[rowIndex][columnIndex+1]
						--print(screenTiles2[rowIndex][columnIndex+1])
						NumstoAvg = NumstoAvg + 1
						--print("pass1")
						end

			if 	rowIndex-1 >= 1 and columnIndex + 1 <= colRes  then
						average = average + screenTiles2[rowIndex-1][columnIndex+1]
						NumstoAvg = NumstoAvg + 1
						--print(screenTiles2[rowIndex-1][columnIndex+1])
						--print("pass2")
						end

			if  rowIndex-1 >= 1 and columnIndex ==columnIndex  then
						average = average + screenTiles2[rowIndex-1][columnIndex]
						NumstoAvg = NumstoAvg + 1
						--print(screenTiles2[rowIndex-1][columnIndex])
						--print("pass3")
						end

			if  rowIndex-1 >= 1 and columnIndex - 1 >= 1  then
						average = average + screenTiles2[rowIndex-1][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass4")
						end

			if  rowIndex == rowIndex and columnIndex -1 >= 1  then
						average = average + screenTiles2[rowIndex][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass5")
						end

			if  rowIndex+1 <= rowRes and columnIndex-1 >= 1 then
						average = average + screenTiles2[rowIndex+1][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass6")
						end

			if  rowIndex+1 <= rowRes and columnIndex ==columnIndex  then
						average = average + screenTiles2[rowIndex+1][columnIndex]
						NumstoAvg = NumstoAvg + 1
						--print("pass7")
						end

			if  rowIndex+1 <= rowRes and columnIndex + 1 <=colRes  then
						average = average + screenTiles2[rowIndex+1][columnIndex+1]
						NumstoAvg = NumstoAvg + 1
						--print("pass8")
			end

			average = average/NumstoAvg
			-- print("AVERAGE".. average.."Nums: "..NumstoAvg)

			screenTiles2[rowIndex][columnIndex] = average

		end
		
	end
	
	end
--before = collectgarbage("count")
expandedmap = {}


--make 2d
	
	for rowIndex = 1, 800 do
		expandedmap[rowIndex] = {}
		for columnIndex = 1, 600 do

			local tileInfo = {}
			tileInfo.value = 0
			tileInfo.type = "grass"

			
			expandedmap[rowIndex][columnIndex] = tileInfo
		end

	end

mapWidth = 800
mapHeight = 600







	--rowIndex for 50X38 matrix Generated previously
	for rowIndex = 0, 49 do
		
		for columnIndex = 0, 37 do

			--read value of 50x38 matrix
			value = screenTiles2[rowIndex+1][columnIndex+1]
			

				--place 16 of above value in new array
				for NEWrowIndex = (rowIndex*16)+1, ((rowIndex*16)+1)+16 do
				
					for NEWcolumnIndex = (columnIndex*16)+1,((columnIndex*16)+1)+16 do 

						if NEWrowIndex <= 800 and NEWcolumnIndex<=600 then
						expandedmap[NEWrowIndex][NEWcolumnIndex].value = value
					   	end
					end



				end

		end

		
	end

-- after = collectgarbage("count")
-- print("before: "..before)
-- print("after: "..after)

-- print(after-before)
	

end





function App:loadControls()
	
	return love.filesystem.load("controls.lua")
end



function App:draw()

	--take player worldcache, and run thru it and only draw those
	-- for i = 1, 9 do

	-- 	local value = expandedmap[player.worldcache][1].value

 
	-- end





	







	--draw expandedmap
	for rowIndex = 1, 800 do
		
		for columnIndex = 1, 600 do 
			


			--io.write(screenTiles2[rowIndex][columnIndex]..",")
			local value = expandedmap[rowIndex][columnIndex].value

			if value < realdeepwater then
				love.graphics.setColor(0,0,70,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > realdeepwater and value <= deepwater then
				love.graphics.setColor(0,0,120,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > deepwater and value <= watercutoff then
				love.graphics.setColor(0,0,150,255)
			love.graphics.point(rowIndex, columnIndex)
			end
			if value > watercutoff and value <= sandcutoff then
				love.graphics.setColor(171,140,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > sandcutoff and value <= lightgrass then
				love.graphics.setColor(123,178,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lightgrass and value <= darkgrass then
				love.graphics.setColor(10,90,30,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > darkgrass and value <= lowermount  then
				love.graphics.setColor(50,50,50,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lowermount and value <=highmount   then
				love.graphics.setColor(150,150,150,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > highmount    then
				love.graphics.setColor(150,150,150,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			
			

			 -- love.graphics.setColor(value,value,value,value)
			 -- love.graphics.point(rowIndex, columnIndex)

		end
		
	end









--draw mini map
	for rowIndex = 1, rowRes do
		
		for columnIndex = 1, colRes do 
			
			--io.write(screenTiles2[rowIndex][columnIndex]..",")
			local value = screenTiles2[rowIndex][columnIndex]
			
			
			if value < realdeepwater then
				love.graphics.setColor(0,0,70,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > realdeepwater and value <= deepwater then
				love.graphics.setColor(0,0,120,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > deepwater and value <= watercutoff then
				love.graphics.setColor(0,0,150,255)
			love.graphics.point(rowIndex, columnIndex)
			end
			if value > watercutoff and value <= sandcutoff then
				love.graphics.setColor(171,140,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > sandcutoff and value <= lightgrass then
				love.graphics.setColor(123,178,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lightgrass and value <= darkgrass then
				love.graphics.setColor(10,90,30,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > darkgrass and value <= lowermount  then
				love.graphics.setColor(50,50,50,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lowermount and value <=highmount   then
				love.graphics.setColor(150,150,150,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > highmount    then
				love.graphics.setColor(150,150,150,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			


			--love.graphics.setColor(value,value,value,value)
 			--love.graphics.point(rowIndex, columnIndex)

 		end
		
 	end

	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.line(0,0,51,0,51,39,0,39,0,0)
	
	
end

function App:update(dt)
	--refresh the cache cords
	player.worldCache = { {player.x,player.y},
						  {player.x+1,player.y},
						  {player.x+1,player.y+1},
						  {player.x,player.y+1},
						  {player.x-1,player.y+1},
						  {player.x-1,player.y},
						  {player.x-1,player.y-1},
						  {player.x,player.y-1},
						  {player.x+1,player.y-1} }



	function love.keypressed(key,isrepeat)
		if key == "w" then
			player.x=player.x+1
			

		end
		if key == "s" then

		end
		if key == "d" then

		end
		if key == "a" then

		end
	end

	mouseX = love.mouse.getX()
	mouseY = love.mouse.getY()
	worldX = math.floor((mouseX/16)+1)
	worldY = math.floor((mouseY/16)+1)
	type=math.floor(expandedmap[mouseX+1][mouseY+1].value)

	

	print("Mouse: "..mouseX..","..mouseY.."      World: "..worldX..","..worldY.."      Value: "..type)
	

	
end
