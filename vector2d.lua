

Vector2d = {}


function Vector2d:new(x,y)
	local newVector2d = {}
	setmetatable(newVector2d, self)
	self.__index = self

	newVector2d.x=x or 0
	newVector2d.y=y or 0

	return newVector2d
end

function Vector2d:add(vector2d)

	self.x=self.x+vector2d.x
	self.y=self.y+vector2d.y
end

function Vector2d:subtract(vector2d)

	self.x=self.x-vector2d.x
	self.y=self.y-vector2d.y
end


function Vector2d:multiply(number)
	self.x=self.x* number
	self.y=self.y* number

end

function Vector2d:divide(number)
	self.x=self.x/number
	self.y=self.y/number

end

function Vector2d:divideout(number)
	self.x=self.x/number
	self.y=self.y/number
	return self

end

function Vector2d:magnitude()
	return math.sqrt((self.x*self.x)+(self.y*self.y))

end

function Vector2d:normalize()

	local mag = self:magnitude()

	if mag ~= 0 then
	self:divide(mag)
	end

end

function Vector2d:SetVector(x,y)
	self.x=x
	self.y=y
end

function Vector2d:Limit(topspeed)

	if self:magnitude() > topspeed then
		self:normalize()
		self:multiply(topspeed)
	end
end



--how do i interact with base x,y?
--if attached to a object i pass in the base
--or does every base entity have vectors?  platform has infinte mass,  