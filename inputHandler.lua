InputHandler = {}

function InputHandler:new(controls)
	local newInputHandler = {}
	setmetatable(newInputHandler, self)
	self.__index = self

	newInputHandler.CurrentKeyPressed = ""
	newInputHandler.CurrentKeyRelease = ""
	--i think input handler should load controls from directory instead of app
	newInputHandler.controls = controls()--i need () so it runs the file, thus returning table of controls
	--print(newInputHandler.controls.up)


	return newInputHandler
end

--on update, check for anything pressed etc
--function check for keyboar input,
--function check for mouse input,
function InputHandler:GetInput()
	--print(self.CurrentKeyPressed)
	return self.CurrentKeyPressed

end

function InputHandler:GetRelease()
	--print(self.CurrentKeyPressed)
	return self.CurrentKeyRelease

end

function InputHandler:ClearInput()
	self.CurrentKeyPressed = ""
end

function InputHandler:update()
	
		if love.keyboard.isDown(self.controls.up) then
			--print("Up")
			self.CurrentKeyPressed=self.controls.up
		end
		if love.keyboard.isDown(self.controls.down)then
			--print("Down")
			self.CurrentKeyPressed=self.controls.down
		end
		if love.keyboard.isDown(self.controls.left)then
			--print("Left")
			self.CurrentKeyPressed=self.controls.left
		end
		if love.keyboard.isDown(self.controls.right)  then
			--print("Right")
			self.CurrentKeyPressed=self.controls.right
		end
		if love.keyboard.isDown(self.controls.jump)  then
			--print("Space")
			self.CurrentKeyPressed=self.controls.jump
		end
		
	function love.keyreleased(key)
		--if w
		if key == self.controls.up then
			--print("Up release")
			self.CurrentKeyPressed=""
		end
		if key == self.controls.down then
			--print("Down release")
			self.CurrentKeyPressed=""
		end
		if key == self.controls.left then
			--print("Left release")
			self.CurrentKeyPressed=""
		end
		if key == self.controls.right then
			--print("Right release")
			self.CurrentKeyPressed=""
		end
		if key == self.controls.space then
			--print("Space release")
			self.CurrentKeyPressed=""
		end
	end
end

-- function InputHandler:KeyboardCheck()
	
-- 		function love.keypressed(key)

-- 			if key == "z"  and love.keyboard.isDown("lgui") then
-- 				self.CurrentKeyPressed="undo"
				
-- 			end

-- 			if key == "s" and love.keyboard.isDown("lgui") then
-- 				self.CurrentKeyPressed = "Saving"
-- 				--press press enter to sav
-- 			end
-- 		end

-- 		return self.CurrentKeyPressed
		 	
-- end
-- mouse input, keyboard,  every frame?  by passing in dt?